import { BsSearch } from "react-icons/bs";
import { filterStateVar } from "../../../state/filterState";
import { debounce } from "../../../service/debounce";
import styles from "./searchInputEl.module.css";
import React, { useMemo, useState } from "react";

export function SearchInputEl() {
  const updateDebounceText = useMemo(
    () =>
      debounce((text: string) => {
        filterStateVar({
          ...filterStateVar(),
          nameSearch: text,
        });
      }, 0),
    []
  );

  const [searchInputValue, setSearchInputValue] = useState<string>("");

  const handleSearchElPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      const target = e.target as HTMLInputElement;
      updateDebounceText(target.value);
      setSearchInputValue(target.value);
    }
  };

  const handleDivClick = (e: React.MouseEvent<HTMLElement>) => {
    updateDebounceText(searchInputValue);
  };

  const handleSearchElChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchInputValue(e.target.value);
  };

  return (
    <div className={styles.searchDiv}>
      <input
        id="searchInput"
        type="text"
        placeholder="Search..."
        value={searchInputValue}
        onKeyDown={handleSearchElPress}
        onChange={handleSearchElChange}
        data-testid="searchBar"
      ></input>
      <div onClick={handleDivClick}>
        <BsSearch
          id="searchButton"
          data-testid="searchButton"
          className={styles.searchIcon}
        />
      </div>
    </div>
  );
}
