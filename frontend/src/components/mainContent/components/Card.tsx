import React, { useState } from "react";
import { GasStation } from "../../../types";
import styles from "./card.module.css";
import circleK from "../../../Images/circleK.png";
import esso from "../../../Images/esso.png";
import shell from "../../../Images/shell.png";
import unoX from "../../../Images/uno-x.png";
import noimage from "../../../Images/noimage.png";
import { BsFillCaretDownFill } from "react-icons/bs";
import { GasStationInfo } from "../../gasStationInfo/GasStationInfo";

export default function Card({ gasStation }: { gasStation: GasStation }) {
  const [collapsed, setCollapsed] = useState(true);
  const [iconIsHovered, setIconIsHovered] = useState(false);
  /**
   *
   * @param number The price to be formatted
   * @returns If not a number, return a string which clarifies that no price was found, else return the formatted price
   */
  function formatPrice(number: number | undefined): string {
    if (!number) {
      return "Ingen pris";
    }
    return number.toFixed(2) + "kr";
  }

  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    if (collapsed) {
      setCollapsed(false);
    } else {
      setCollapsed(true);
    }
  };

  return (
    <div className={styles.wrapper}>
      <div
        onClick={handleClick}
        className={styles.cardStyle}
        id="gasStationLink"
        data-testid="gasStationEl"
      >
        <BsFillCaretDownFill className={styles.cardIcon} />
        <div className={styles.imageDiv}>
          <img
            className={styles.cardStyleImage}
            src={findImage(gasStation.name)}
            alt={gasStation.name + " logo"}
          />
        </div>
        <div className={styles.cardInformation}>
          <div className={styles.cardAreaDiv}>
            <span className={styles.cardBrand} data-testid="gasStationName">
              {gasStation.name}
            </span>
            <span className={styles.cardArea}>{gasStation.city}</span>
          </div>
          <div className={styles.cardPriceDiv}>
            <span data-cy="cardPrice" className={styles.cardPrice}>
              {formatPrice(gasStation.latestPrice)}
            </span>
          </div>
        </div>
        <div></div>
      </div>
      {!collapsed ? (
        <div>
          <GasStationInfo id={gasStation.id} />
        </div>
      ) : null}
    </div>
  );

  /**
   *
   * @param brandName The name of the gasStation
   * @returns Returns the image related to the name of the gasStation
   */
  function findImage(brandName: string): string | undefined {
    if (brandName === "Esso") {
      return esso;
    } else if (brandName === "Shell") {
      return shell;
    } else if (brandName === "Circle K") {
      return circleK;
    } else if (brandName === "Uno-X") {
      return unoX;
    }
    return noimage;
  }
}
