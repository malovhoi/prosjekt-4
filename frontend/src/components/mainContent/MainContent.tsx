import { useQuery, useReactiveVar } from "@apollo/client";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import styles from "./maincontent.module.css";

import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";
import { GET_GAS_STATIONS } from "../../graphql/queries.graphql";
import {
  hasMoreVar as endlessScrollHasMoreElementsVar,
  limit,
} from "../../state/endlessScrollState";
import { filterStateVar } from "../../state/filterState";
import { GasStation, GetGasStationsData } from "../../types";
import { useEffect } from "react";
import FilterEl from "./components/FilterEl";
import { SearchInputEl } from "./components/SearchInputEl";
import { GasStationInfo } from "../gasStationInfo/GasStationInfo";
import Card from "./components/Card";

export default function MainContent() {
  // Reactive variable used to track filter state
  const filterState = useReactiveVar(filterStateVar);

  // Reactive variable which tracks if there are more elements to load
  const endlessScrollHasMoreElements = useReactiveVar(
    endlessScrollHasMoreElementsVar
  );

  /**
   * Fetches data from graphql server
   */
  const { error, loading, data, fetchMore, refetch } =
    useQuery<GetGasStationsData>(GET_GAS_STATIONS, {
      variables: {
        city: filterState.city,
        maxPrice: filterState.maxPrice,
        nameSearch: filterState.nameSearch,
        sortBy: filterState.sortBy,
        sortDirection: filterState.sortDirection,
        limit,
      },
    });

  useEffect(() => {
    endlessScrollHasMoreElementsVar(true);
  }, [filterState]);

  function loadMoreData() {
    fetchMore({
      variables: {
        skip: data?.gasStations.length,
      },
    });
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <main className={styles.main}>
      <div className={styles.filterDiv}>
        <SearchInputEl />
        <FilterEl />
      </div>
      {data && !loading ? (
        <InfiniteScroll
          className={styles.mainContent}
          next={loadMoreData}
          hasMore={endlessScrollHasMoreElements}
          children={data.gasStations.map((gasStation) => (
            <Card gasStation={gasStation} key={gasStation.id} />
          ))}
          loader={<h4>Loading...</h4>}
          dataLength={data.gasStations.length}
        />
      ) : (
        <AiOutlineLoading3Quarters size={20} />
      )}
      {data && data.gasStations.length === 0 && !loading && (
        <h4 className="center margin">Ingen bensinstasjoner i valgt søk</h4>
      )}
      {!endlessScrollHasMoreElements && data && data.gasStations.length > 0 && (
        <h4 className="center margin">Ingen flere bensinstasjoner ⛽</h4>
      )}
    </main>
  );
}
