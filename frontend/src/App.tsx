import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ApolloWrapper from "./ApolloWrapper";
import Frontpage from "./components/frontpage/Frontpage";

function App() {
  return (
    <ApolloWrapper>
      <BrowserRouter basename="/project3">
        <Routes>
          <Route path="/" element={<Frontpage />} />
        </Routes>
      </BrowserRouter>
    </ApolloWrapper>
  );
}

export default App;
